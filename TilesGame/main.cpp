#include "Board.h"
#include "Game.h"

#include "Position.h"
int main()
{
	//Position pos;
	std::ifstream fileIn("salut.txt");
	Board<int> myBoard(fileIn);
	fileIn.close();
	fileIn.open("unsolvable.txt");
	Board<int> myOtherBoard(fileIn);

	fileIn.close();
	fileIn.open("goal.txt");
	Board<int> goalBoard(fileIn);

	auto y = myOtherBoard.swap(directions::Direction::Up);
	auto x = (myBoard == myOtherBoard);
	auto isSolvable = myOtherBoard.isSolvable();


	auto myHash = std::hash<Board<int>>{}(myBoard);

	if (!myBoard.isSolvable())
	{
		std::cerr << "The board is not solvable.\n";
		return -1;
	}

	Game<int> myGame(myBoard, goalBoard);
	myGame.Generate();
	auto path = myGame.ReconstructPath();
	std::ofstream outputFileStream("out.txt");
	std::for_each(std::crbegin(path), std::crend(path), [&outputFileStream](const Board<int>& board) {board.print(outputFileStream); });
	
	return 0;
}