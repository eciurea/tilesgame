#pragma once

struct Position
{
	constexpr Position(int r, int c)
		: row(r),
		 column(c)
	{}

	Position() = default;

	Position& operator + (const Position& other)
	{
		row += other.row;
		column += other.column;
		return *this;
	}

	Position& operator += (const Position& other)
	{
		*this = *this + other;
		return *this;	
	}

	int row;
	int column;
};

constexpr bool operator == (const Position& lhs, const Position& rhs) noexcept
{
	return (lhs.column == rhs.column && lhs.row == rhs.row);
}

constexpr bool operator != (const Position& lhs, const Position& rhs) noexcept
{
	return (lhs.column != rhs.column || lhs.row != rhs.row);
}