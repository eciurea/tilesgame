#pragma once

#include <vector>
#include <fstream>
#include <iostream>
#include <string>
#include <stdexcept>
#include <sstream>

#include "Position.h"
#include "Utilities.h"
#include "Direction.h"

using directions::Direction;

template <typename T>
class Board
{
public:
	static constexpr int kSentinelValue = 9;
	static constexpr int kLowerBound = 0;
public:
	Board(std::istream& inputStream);
	bool operator == (const Board& other) const;
	Board swap(Direction direction);
	constexpr const Position& sentinelPosition() const noexcept;
	constexpr const std::vector<std::vector<T>>& board() const noexcept;
	bool isSolvable() const;

	void print(std::ostream& os) const;

private:
	void build(std::istream& inputStream);

	bool isPositionValid(const Position& position) const;

	Position _sentinelPosition;
	std::vector<std::vector<T>> _board;
};

namespace std
{
	template <typename T> 
	struct hash<Board<T>>
	{
		std::size_t operator()(const Board<T>& board) const noexcept
		{
			std::string boardToString;
			for (const auto& row : board.board())
			{
				for (const auto& element : row)
					boardToString += std::to_string(element);
			}

			return std::hash<std::string>{}(boardToString);
		}
	};

}

template<typename T>
inline Board<T>::Board(std::istream & inputStream)
{
	build(inputStream);
}

template<typename T>
bool Board<T>::operator==(const Board& other) const
{
	if (_sentinelPosition != other._sentinelPosition)
	{
		return false;
	}

	for (int row = 0; row < _board.size(); ++row)
	{
		for (int column = 0; column < _board.size(); ++column)
		{
			if (_board[row][column] != other._board[row][column])
			{
				return false;
			}
		}
	}

	return true;
}

template<typename T>
inline Board<T> Board<T>::swap(Direction direction)
{
	Board copy = *this;

	auto offsets = directions::directionOffsets.at(direction);
	auto temporaryPosition = copy.sentinelPosition();
	temporaryPosition += offsets;

	if (isPositionValid(temporaryPosition))
	{
		auto copyPosition = copy._sentinelPosition;
		std::swap(copy._board[copyPosition.row][copyPosition.column]
			, copy._board[temporaryPosition.row][temporaryPosition.column]);
		copy._sentinelPosition = temporaryPosition;
	}
	else
		throw std::invalid_argument("Invalid direction.");

	return copy;
}

template<typename T>
inline constexpr const Position & Board<T>::sentinelPosition() const noexcept
{
	return _sentinelPosition;
}

template<typename T>
inline constexpr const std::vector<std::vector<T>>& Board<T>::board() const noexcept
{
	return _board;
}

template<typename T>
inline bool Board<T>::isSolvable() const
{
	std::vector<T> flattened;
	for (const auto& row : _board)
	{
		for (const auto& element : row)
			flattened.emplace_back(element);
	}

	return isSolvabale(flattened, _board.size());
}

template<typename T>
inline void Board<T>::print(std::ostream& os) const
{
	for (int row = 0; row < _board.size(); ++row)
	{
		for (int column = 0; column < _board[row].size(); ++column)
		{
			os << _board[row][column] << ' ';
		}
		os << '\n';
	}
	os << '\n';
}

template<typename T>
inline void Board<T>::build(std::istream & inputStream)
{
	int boardSize{ 0 };
	std::string currentLine;
	std::getline(inputStream, currentLine);
	try
	{
		boardSize = std::stoi(currentLine);
	}
	catch (const std::invalid_argument& e)
	{
		std::cerr << e.what() << '\n';
		return;
	} 

	_board.reserve(boardSize);
	for (int row = 0; row < boardSize; ++row)
	{	
		std::getline(inputStream, currentLine);
		std::istringstream lineParser(currentLine);
		std::vector<T> currentRow;
		int currentValue{ 0 };

		currentRow.reserve(boardSize);
		for (int column = 0; column < boardSize; ++column)
		{	
			lineParser >> currentValue;
			if (currentValue == kSentinelValue)
			{
				_sentinelPosition.row = row;
				_sentinelPosition.column = column;
			}
			currentRow.emplace_back(currentValue);
		}

		_board.push_back(currentRow);
	}
}

template<typename T>
inline bool Board<T>::isPositionValid(const Position& position) const
{
	auto hasUnderflown = (position.row < kLowerBound) || (position.column < kLowerBound);
	if (hasUnderflown)
		return false;

	static const auto kUpperBound = _board.size() - 1;
	auto hasOverflown = (position.row > kUpperBound) || (position.column > kUpperBound);
	if (hasOverflown)
		return false;

	return true;
}
