#pragma once

#include <algorithm>
#include <vector>

bool myComparator(int a, int b)
{
	return a > b;
}


unsigned numberOfInversions(const std::vector<int>& vector, int length)
{
	unsigned inversions = 0;
	for (auto it = vector.begin(); it < vector.end(); ++it)
	{
		unsigned counter = 0;
		if (*it != length * length)
		{
			counter = std::count_if(it, vector.end(), [it](int a) {return a < *it; });
			inversions += counter;
		}
	}
	return inversions;
}

bool blankOnRow(const std::vector<int>& vector, int length)
{
	for (int index = 0; index < vector.size(); ++index)
	{
		if (vector[index] == length * length && (index / length + 1) % 2 == 0)
			return 1; //is on even row
	}
	return 0;// is on odd row
}

bool isSolvabale(const std::vector<int>& vector, int length)
{
	auto inversions = numberOfInversions(vector, length);
	return (((length % 2 != 0) && (inversions % 2 == 0))
		|| ((length % 2 == 0) && blankOnRow(vector, length) != 0 && (inversions % 2 != 0))
		|| ((length % 2 == 0) && blankOnRow(vector, length) == 0 && (inversions % 2 == 0)));
}