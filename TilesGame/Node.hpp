#pragma once
#include <memory>
#include "Board.h"

template<typename T>
class Node
{
public:
	Node(Board<T> board, std::weak_ptr<Node> parent);
	void addChild( std::shared_ptr<Node> child);
private:
	Board<T> _board;
	std::weak_ptr<Node> _parent;
	std::vector<std::shared_ptr<Node>> _children;
};

template<typename T>
inline Node<T>::Node(Board<T> board, std::weak_ptr<Node> parent)
	:_board(board),
	_parent(parent)
{}

template<typename T>
inline void Node<T>::addChild(std::shared_ptr<Node> child)
{
	_children.push_back(child);
}
