#pragma once
#include "Board.h"

#include <queue>
#include <unordered_set>
#include <array>
template <typename T>
class Game
{
public:
	inline static constexpr std::array<directions::Direction, 4> kDirections{ Direction::Up, Direction::Down, Direction::Left, Direction::Right };
public:
	Game(const Board<T>& startState, const Board<T>& goalState);
	
	void Generate();
	std::vector<Board<T>> ReconstructPath();
private:
	std::queue<Board<T>> _toBeChecked; //fringed
	std::unordered_set<Board<T>> _visited;  //closed
	std::unordered_map<Board<T>, Board<T>> _cameFrom;
	Board<T> _goalState;
	Board<T> _sourceState;
	std::vector<Board<T>> _path;
};

template<typename T>
inline Game<T>::Game(const Board<T>& startState, const Board<T>& goalState)
	:_goalState(goalState),
	_sourceState(startState)
{
	_toBeChecked.push(startState);
}

template<typename T>
inline void Game<T>::Generate()
{
	_cameFrom.insert({ _toBeChecked.front(), _toBeChecked.front() });
	while (!_toBeChecked.empty())
	{
		auto currentBoard = _toBeChecked.front();
		_visited.insert(currentBoard);
		_toBeChecked.pop();

		if (currentBoard == _goalState)
		{
			std::cout << "S-a dat gol.\n";
			return;
		}
		
		for (auto direction : kDirections)
		{
			try
			{
				auto newState = currentBoard.swap(direction);
				auto it = _visited.find(newState);
				if ( it == std::end(_visited))
				{
					_toBeChecked.push(newState);
					_cameFrom.insert({ newState, currentBoard });
				}
			}
			catch (const std::invalid_argument&)
			{
				continue;
			}
		}	
	}
}

template<typename T>
inline std::vector<Board<T>> Game<T>::ReconstructPath()
{
	auto currentState = _goalState;
	while (!(currentState == _sourceState))
	{
		_path.push_back(currentState);
		currentState = _cameFrom.at(currentState);
	}

	_path.push_back(_sourceState);
	return _path;
}
