#pragma once
#include <cstdint>
#include <unordered_map>

#include "Position.h"

namespace directions
{
	enum class Direction : std::uint8_t
	{
		Up,
		Down,
		Left,
		Right
	};

	const auto directionOffsets = std::unordered_map<Direction, Position>
	{
		{Direction::Up, {-1, 0}},
		{Direction::Right, {0, 1}},
		{Direction::Down, {1, 0}},
		{Direction::Left, {0, -1}}
	};
}
