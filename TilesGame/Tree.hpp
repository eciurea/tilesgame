#pragma once

#include "Node.hpp"

template <typename T>
class Tree
{
public:
	Tree(const std::shared_ptr<Node<T>> root);
	//void 
private:
	std::shared_ptr<Node<T>> _root;
};

template<typename T>
inline Tree<T>::Tree(const std::shared_ptr<Node<T>> root)
	: _root(root)
{}


